export const fieldChanged = (values) => {
    return {
        type: 'CHANGE-FIELD',
        payload: values,
    };
};