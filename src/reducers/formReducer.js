const initialState = {
    cardCvv: "",
    cardExpireDate: "",
    cardName: "",
    cardNumber: "",
    name: "",
    payment: "",
    cvvActive: false,
    isFormOk: false,
};

export const formReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'CHANGE-FIELD':
            return action.payload;

        default:
            return state;
    }
};