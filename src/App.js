import React from 'react';

import TitleBar from './components/title-bar/TitleBar';
import CardForm from './components/card-form/CardForm';

import './App.scss';
import './utils/global-style.scss';

export default () => {
  return (
    <div className="App">
        <TitleBar/>
        <CardForm/>
    </div>
  );
}
