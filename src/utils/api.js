import axios from 'axios';

export const axiosInstance = axios.create({
  baseURL: 'https://URL-INICIAL-DA-API-DA-BENX',
});

export const get = (path, params={}) => axiosInstance(path, { params: params });

export const post = (path, data) => axiosInstance(path, { method: 'POST', data });
