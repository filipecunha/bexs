import React from 'react';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import styled from 'styled-components';

import './select.scss'

const colors = {
    gray: '#C9C9C9',
    grayer: '#3C3C3C',
    orange: '#DE4B4B',
};

const StyledInput = styled(({ ...props }) => <Select
    {...props}
/>)`
    & .MuiSelect-select {
        border-bottom: 2px solid ${colors.gray};
    }
`;

const _Select = (props) => (
    <FormControl className={props.className}>
        <StyledInput {...props}/>

        <img className="arrow-icon" src="./assets/img/arrow-down.png" alt=""/>

        <div className="error-message">
            <span>{props['error-message']}</span>
        </div>
    </FormControl>
);

export default _Select;