import React from 'react';

import './title-bar.scss';
import '../../utils/global-style.scss';
import { replaceAll } from "../../utils/helpers";
import { connect } from 'react-redux';

const TitleBar = (props) => {

    const formatCardNumber = (value) => {
        let result = replaceAll(value,' ', '');

        const charsMissing = 16 - result.length;

        for(let i=0; i< charsMissing; i++) {
            result = result + '*'
        }

        const block1 = result.substr(0, 4);
        const block2 = result.substr(4, 4);
        const block3 = result.substr(8, 4);
        const block4 = result.substr(12, 4);

        return `${block1} ${block2} ${block3} ${block4}`;
    };

    const formatCardName = (value) => {
        if(value === '')
            return 'NOME DO TITULAR';
        else
            return value;
    };

    const formatValidation = (value) => {
        const result = value.replace( /^\D+/g, '');

        if(result === '' || result === '  /  ')
            return '00/00';
        else
            return result.substr(0, 5);
    };

    const formatCvv = (value) => {
        const charsMissing = 3 - value.length;
        let result = value.replace( /^\D+/g, '');

        for(let i=0; i< charsMissing; i++) {
            result = result + '*'
        }

        return result.substr(0, 3);
    };

    return (
        <div className="title-bar">
            <div className="back-button">
                <img className="arrow-left" src="/assets/img/arrow-left.png" alt=""/>
                <span className="back-button-text">Alterar forma de pagamento</span>
            </div>
            <div className="steps">
                <strong>Etapa 2</strong> de 3
            </div>
            <div className="card-title">
                <h2>
                    <img className="card-icon" src="/assets/img/icon-card.svg" alt=""/>
                    <span className="title">Adicione um novo cartão de crédito</span>
                </h2>
            </div>
            <div className={`card-image ${props.form.cvvActive ? 'card-image--flip' : ''}`}>
                <div className="card-inner">
                    <div className="card-front">
                        <div className="card-number">{formatCardNumber(props.form.cardNumber)}</div>
                        <div className="owner-name">{formatCardName(props.form.cardName)}</div>
                        <div className="expiration-date">{formatValidation(props.form.cardExpireDate)}</div>
                    </div>
                    <div className="card-back">
                        <div className="cvv">{formatCvv(props.form.cardCvv)}</div>
                    </div>
                </div>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        form: state.form,
    }
};

export default connect(mapStateToProps, null)(TitleBar);