import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

// We can inject some CSS into the DOM.
const styles = {
    root: {
        width: '246px',
        height: '51px',
        borderRadius: '10px',
        backgroundColor: '#DE4B4B',
        color: 'white',
        fontWeight: 'bold',
        fontSize: '17px',
    },
};

function ClassNames(props) {
    const { classes, children, className, ...other } = props;

    return (
        <Button className={clsx(classes.root, className)} {...other}>
            {children}
        </Button>
    );
}

ClassNames.propTypes = {
    children: PropTypes.node,
    classes: PropTypes.object.isRequired,
    className: PropTypes.string,
};

export default withStyles(styles)(ClassNames);