import React from 'react';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import styled from 'styled-components';

import './TextField.scss';

const colors = {
    gray: '#C9C9C9',
    grayer: '#3C3C3C',
    orange: '#DE4B4B',
};

const StyledInput = styled(({ ...props }) => <TextField {...props} />)`    
    & .MuiInputBase-input, .MuiInput-input {
        font-size: 17px;
        font-family: 'Verdana';
        color: '#3C3C3C' !important;
    }
    
    & .MuiInputBase-input {
        color: ${colors.grayer};
    }
    
    & .MuiFormLabel-root.Mui-focused {
        color: ${colors.gray};
    }
    
    $ .MuiInputLabel-root {
        color: red;
    }
    
    $ .MuiInputLabel-shrink {
        font-size: 20px !important;
        color: red !important;
    }
    
    & .MuiInputLabel-root, .MuiInputLabel-skrink {
        color: ${colors.gray};
    }
    
    & .MuiInput-formControl::hover {
        border-bottom: 2px solid ${colors.gray};
    }
    
    & .MuiInput-underline {
        border-bottom: 2px solid ${colors.gray};
    }
    
    & .MuiInput-underline::after {
        display: none;
    }
    
    & .MuiInput-underline::before {
        display: none;
    }
    
    & .MuiInput-underline::hover {
        display: none !important;
    }
    
    & .MuiInput-root {
        color: black;
        font-family: 'Roboto';
        font-weight: 300;
    }  
`;

const TextInput = (props) => {
    // console.log(props);
    return(
        <FormControl className={props.className}>
            <StyledInput {...props}/>

            <div className="error-message">
                <span>{props['error-message']}</span>
            </div>
        </FormControl>
    );
}

export default TextInput;