import React, { useState } from 'react';
import {makeStyles} from '@material-ui/core/styles';
import { connect } from 'react-redux';
import MenuItem from "@material-ui/core/MenuItem";
import InputMask from 'react-input-mask';

import TextField from '../../components/textfiled/TextField';
import Button from '../../components/button/Button';
import Select from '../../components/select/Select';
import '../../utils/global-style.scss';
import './card-form.scss'
import { post } from '../../utils/api';
import { replaceAll } from "../../utils/helpers";

import { fieldChanged } from '../../actions';


const CardForm = (props) => {
    const useStyles = makeStyles(() => ({
        root: {
            width: '100%'
        },
        fullWidth: {
            width: '100%',
        },
        halfWidth: {
            width: 210,
        },
        regularMarginTop: {
            marginTop: '8px',
        },
        marginRight: {
            marginRight: '20px',
        },
        biggerMarginTop: {
            marginTop: '16px',
        },
        submitBtn: {
            marginTop: '40px',
        },
    }));

    const classes = useStyles();

    const [errors, setErrors] = useState({
        cardNumber: '',
        cardName: '',
        cardExpireDate: '',
        cardCvv: '',
        payment: ''
    });

    const handleChange = name => event => {
        props.fieldChanged({
            ...props.form,
            [name]: event.target.value
        });
    };
    
    const onSubmit = () => {
        let canSubmit = true;

        const _errors = {
            cardNumber: '',
            cardName: '',
            cardExpireDate: '',
            cardCvv: '',
            payment: ''
        };

        const cardNumberValidation = validateCardNumber();
        const cardNameValidation = validateCardName();
        const cardExpireDateValidation = validateCardExpireDate();
        const cardCvvValidation = validateCardCvv();
        const paymentValidation = validatePayment();

        if( cardNumberValidation !== '') {
            canSubmit = false;
            _errors.cardNumber = cardNumberValidation;
        }

        if( cardNameValidation !== '') {
            canSubmit = false;
            _errors.cardName = cardNameValidation;
        }

        if( cardExpireDateValidation !== '') {
            canSubmit = false;
            _errors.cardExpireDate = cardExpireDateValidation;
        }

        if( cardCvvValidation !== '') {
            canSubmit = false;
            _errors.cardCvv = cardCvvValidation;
        }

        if( paymentValidation !== '') {
            canSubmit = false;
            _errors.payment = paymentValidation;
        }

        setErrors(_errors);
        
        if(canSubmit) {
            post('/ENDPOINT')
                .then(() => {
                    // Aqui entra o código de feedback de sucesso dos dados enviados,
                    // mas nunca aserá execultado porque a URL não existe nesse teste
                })
                .catch(() => {
                    console.log('API chamada, mas o erro é lançado porque a URL chamada é inválida nesse teste');
                })
        }
    };

    const validateCardNumber = () => {
        const value = props.form.cardNumber;
        const result = replaceAll(value, ' ', '');

        if(result.length < 16) {
            return 'Número de cartão inválido';
        }

        return  '';
    };

    const validateCardName = () => {
        const value = props.form.cardName;
        const result = replaceAll(value, ' ', '');

        if(result.length < 3){
            return 'Insira seu nome completo';
        }
        return ''
    };

    const validateCardExpireDate = () => {
        const value = props.form.cardExpireDate;
        const result = replaceAll(value, ' ', '');

        if(result.length === 5) {
            const month = parseInt(result.substr(0, 2));
            const year = parseInt(result.substr(3));

            if(month < 1 || month > 12) {
                return 'Mês inválido';
            }
            else if(year < 19 || year > 30) {
                return 'Ano inválido'
            }
        } else {
            return 'Data inválida';
        };

        return ''
    };

    const validateCardCvv = () => {
        const value = props.form.cardCvv;
        const result = replaceAll(value, ' ', '');

        if (result.length < 3) {
            return 'Código inválido'
        }

        return '';
    };

    const validatePayment = () => {
        const value = props.form.payment;

        if (value === '')
            return 'Escolha o número de parcelas';
        else
            return '';
    };
    
    const validate = (name) => {
        let errorMessage = '';

            switch (name) {
            case 'cardNumber':
                errorMessage = validateCardNumber();
                break;

            case 'cardName':
                errorMessage = validateCardName();
                break;

            case 'cardExpireDate':
                errorMessage = validateCardExpireDate();
                break;

            case 'cardCvv':
                errorMessage = validateCardCvv();
                break;

            default:
                errorMessage = ''
        }

        setErrors({
            ...errors,
            [name]: errorMessage
        })
    };

    const handleBlur = (evt) => {
        const target = evt.target;
        const name = target.name;
        const value = target.value;
        
        validate(name, value);
    };

    return (
        <div className="card-form">
            <div className="bread-crumb">
                <div className="step">
                    <img className="check" src="/assets/img/check.png" alt=""/>
                    <span className="step-label">Carrinho</span>
                    <img className="arrow-right" src="/assets/img/arrow-right.png" alt=""/>
                </div>
                <div className="step">
                    <div className="step-number">2</div>
                    <span className="step-label">Pagamento</span>
                    <img className="arrow-right" src="/assets/img/arrow-right.png" alt=""/>
                </div>
                <div className="step">
                    <div className="step-number">3</div>
                    <span className="step-label">Confirmação</span>
                    <img className="arrow-right" src="/assets/img/arrow-right.png" alt=""/>
                </div>
            </div>
            <form className="form" autoComplete="off">
                <div className={classes.root}>
                    <InputMask
                        mask="9999 9999 9999 9999"
                        maskChar={' '}
                        alwaysShowMask={false}
                        className="fullWidth"
                        name="cardNumber"
                        label="Número do cartão"
                        error-message={errors.cardNumber}
                        onInput={handleChange('cardNumber')}
                        onBlur={handleBlur}
                    >{(inputProps) => <TextField {...inputProps}/>}
                    </InputMask>
                    <TextField
                        name="cardName"
                        type="text"
                        label="Nome (igual ao cartão)"
                        className={[classes.fullWidth, classes.regularMarginTop].join(' ')}
                        onInput={handleChange('cardName')}
                        value={props.form.cardName}
                        autoComplete="off"
                        inputProps={{ maxLength: 24 }}
                        error-message={errors.cardName}
                        onBlur={handleBlur}
                    />
                    <InputMask
                        className={[classes.halfWidth, classes.regularMarginTop, classes.marginRight].join(' ')}
                        mask="99/99"
                        maskChar={' '}
                        alwaysShowMask={false}
                        name="cardExpireDate"
                        label="Validade"
                        onInput={handleChange('cardExpireDate')}
                        error-message={errors.cardExpireDate}
                        onBlur={handleBlur}
                    >{(inputProps) => <TextField {...inputProps}/>}
                    </InputMask>
                    <InputMask
                        className={[classes.halfWidth, classes.regularMarginTop].join(' ')}
                        mask="999"
                        maskChar={' '}
                        alwaysShowMask={false}
                        name="cardCvv"
                        label="CVV "
                        onInput={handleChange('cardCvv')}
                        error-message={errors.cardCvv}
                        onFocus={() => {
                            props.fieldChanged({
                                ...props.form,
                                cvvActive: true
                            });
                        }}
                        onBlur={(evt) => {
                            props.fieldChanged({
                                ...props.form,
                                cvvActive: false
                            });
                            handleBlur(evt);
                        }}
                    >{(inputProps) => <TextField {...inputProps}/>}
                    </InputMask>
                    <Select
                        value={props.form.payment}
                        onChange={handleChange('payment')}
                        className={[classes.fullWidth, classes.biggerMarginTop].join(' ')}
                        inputProps={{
                            name: 'payment',
                            id: 'payment',
                        }}
                        error-message={errors.payment}
                    >
                        <MenuItem value={1}>1X R$12.000,00</MenuItem>
                        <MenuItem value={2}>3x R$4.000,00 sem juros</MenuItem>
                        <MenuItem value={3}>12x R$1.000,00 sem juros</MenuItem>
                    </Select>
                    <Button
                        className={classes.submitBtn}
                        onClick={onSubmit}
                    >
                        CONTINUAR
                    </Button>
                </div>
            </form>

            {/*<TextField
                id="maskExample"
                label="Mask Example"
                // className={classes.textField}
                // margin="full"
                InputProps={{
                    inputComponent: CardNumberMask
                }}
            />*/}

            {/*<InputMask
                mask="9999 9999 9999 9999"
                maskChar={' '}
                value={props.value}
                onChange={props.onChange}
                alwaysShowMask={false}
            >
                {
                    (inputProps) => {
                        return (
                            <TextField {...inputProps} />
                        )
                    }
                }
            </InputMask>*/}
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        form: state.form,
    }
};

export default connect(mapStateToProps, { fieldChanged })(CardForm);