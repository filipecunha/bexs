## Como rodar o projeto localmente

Para testar localmente, abra o terminal na pasta do projeto e use o comando:

### `npm install`

Esse comando instala todas as dependências do projeto.

Para rodar o projeto localmente, use o comando:

### `npm start`

Após rodar o comando, seu navegador padrão deve abrir a página [http://localhost:3000](localhost:3000).<br>
Caso ele não abra automaticamente, entree essa url no seu navegador. 
<br>
<br>
<br>
## Sobre o projeto

O projeto demorou cerca de 13 horas para ser feito e utiliza as seguintes libraries 

- react: Framework de desenvolvimento Javascript
- redux: Lib de gereciamento de estado do aplicativo
- material-ui: Framework de elementos visuais para o formulário
- axios: Lib para chamadas assíncronas
- node-sass: Lib para interpretação do SASS (pre-processador CSS)
- react-input-mask: Lib para utilização de máscaras nos campos do formulário